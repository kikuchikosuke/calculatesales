package jp.alhinc.kikuchi_kosuke.calculate_sales;

//パッケージ
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class CalculateSales {
	public static void main(String[] args){
		if(args.length !=1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		Map<String,String>lstMap =new HashMap<>();
		Map<String,Long>codeMap =new HashMap<>();

		//支店定義ファイル読み込みメソッド呼び出し
		if(!inputMethod(args[0],"branch.lst","支店定義ファイル","[0-9]{3}",lstMap,codeMap)){
			return;
		}
		//計算メソッド呼び出し
		if(!calMethod(args[0],codeMap)){
			return;
		}
		//集計ファイル書き込みメソッド呼び出し
		if(!outputMethod(args[0], "branch.out",lstMap,codeMap)){
			return;
		}
	}

	//支店定義ファイル読み込みメソッド
	static boolean inputMethod(String commandLine,String inputName,
							   String lstName,String matches,
							   Map<String,String> lstMap,Map<String,Long>codeMap){
		BufferedReader br = null;
		File file = null;
		String line = null;

		try{
			//branch.lstからmapとkeyにbrunchlstファイルを読み込むプログラム
			file = new File(commandLine ,inputName);
			if(!file.exists()){
				System.out.println(lstName+"が存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			while((line = br.readLine()) != null){
				String[] branchlst = line.split(",");
				if(branchlst[0].matches(matches) && branchlst.length == 2){
					lstMap.put(branchlst[0],branchlst[1]);
					codeMap.put(branchlst[0], 0L);
				}else{
					System.out.println(lstName+"のフォーマットが不正です");
					return false;
				}
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(br != null){
				try {
					br.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//計算メソッド
	static boolean calMethod(String commandLine,Map<String,Long>codeMap){
		File dir = new File(commandLine);
		File[] lists = dir.listFiles();
		List<File> fileLists = new ArrayList<>();
		if(lists != null){
			for(int i=0; i<lists.length; i++){
				String rcd =lists[i].getName();
				if(rcd.matches("[0-9]{8}.rcd$")){
					if(lists[i].isFile()){
						fileLists.add(lists[i]);
					}else{
						System.out.println("売上ファイル名が連番になっていません");
						return false;
					}
				}
			}
		}for(int i = 0; i <fileLists.size()-1;i++){
			String check =fileLists.get(i).getName().substring(0,8);
			String check2 = fileLists.get(i+1).getName().substring(0,8);
			int beforeFile = Integer.parseInt(check);
			int afterFile = Integer.parseInt(check2);
			if(afterFile-beforeFile != 1 ){
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}
		}
		if(fileLists != null){
			for(int i =0; i<fileLists.size(); i++){
				List<String> contentLists = new ArrayList<>();
				BufferedReader br2 = null;
				try{
					FileReader fr2 = new FileReader(fileLists.get(i));
					br2 = new BufferedReader(fr2);
					String val;
					while((val = br2.readLine()) !=null){
						contentLists.add(val);

					}
					if(!codeMap.containsKey(contentLists.get(0))){
						System.out.println(fileLists.get(i).getName()+"の支店コードが不正です");
						return false;
					}
					if(contentLists.size() !=2){
						System.out.println(fileLists.get(i).getName()+"のフォーマットが不正です");
						return false;
					}
					if(!contentLists.get(1).matches("[0-9]+")){
							System.out.println("予期せぬエラーが発生しました");
							return false;
					}
					long sum = codeMap.get(contentLists.get(0));
					sum += Long.parseLong(contentLists.get(1));
					if(sum>=10000000000L){
						System.out.println("合計金額が10桁を超えました");
						return false;
					}
					codeMap.put(contentLists.get(0),sum);
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}finally{
					if(br2 != null){
						try {
							br2.close();
						}catch(IOException e){
							System.out.println("予期せぬエラーが発生しました");
						}
					}
				}
			}
		}
		return true;
	}

	//集計ファイル書き込みメソッド
	static boolean outputMethod(String commandLine,String outputName,
							    Map<String,String> lstMap,Map<String,Long>codeMap){
		BufferedWriter bw = null;
		try{
			File branchout = new File(commandLine,outputName);
			FileWriter fw = new FileWriter(branchout);
			bw = new BufferedWriter(fw);
			for(Map.Entry<String,String>entry:lstMap.entrySet()){
				bw.write(entry.getKey()+","+entry.getValue()+","+codeMap.get(entry.getKey()));
				bw.newLine();
				System.out.println(entry.getKey()+","+entry.getValue()+","+codeMap.get(entry.getKey()));
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(bw != null){
				try {
					bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
	return true;
	}
}


